#!/usr/bin/env bash

DEV_PACKAGES='roave/security-advisories:dev-latest'

echo ""
echo "Please tell me which dev dependencies you want to install."
echo ""
echo "Dependencies to install :"

while [[ $IDE_HELPER != "y" && $IDE_HELPER != "n" ]]; do
  read -p " barryvdh/laravel-ide-helper [y/n]: " -e IDE_HELPER
done

while [[ $COMPOSER_GIT_HOOKS != "y" && $COMPOSER_GIT_HOOKS != "n" ]]; do
  read -p " brainmaestro/composer-git-hooks [y/n]: " -e COMPOSER_GIT_HOOKS
done

while [[ $DEBUGBAR != "y" && $DEBUGBAR != "n" ]]; do
  read -p " barryvdh/laravel-debugbar [y/n]: " -e DEBUGBAR
done

if [[ "$IDE_HELPER" = 'y' ]]; then
  DEV_PACKAGES="$DEV_PACKAGES barryvdh/laravel-ide-helper"
fi

if [[ "$COMPOSER_GIT_HOOKS" = 'y' ]]; then
  DEV_PACKAGES="$DEV_PACKAGES brainmaestro/composer-git-hooks"
fi

if [[ "$DEBUGBAR" = 'y' ]]; then
  DEV_PACKAGES="$DEV_PACKAGES barryvdh/laravel-debugbar"
fi
# Change minimum stability for installing roave/security-advisories
sed -i '' 's/"minimum-stability".*/"minimum-stability": "dev",/' composer.json

# Install dev dependencies
composer require --dev $DEV_PACKAGES

if [[ "$IDE_HELPER" = 'y' ]]; then
  echo "Copying config for barryvdh/laravel-ide-helper"
  # TODO: copy module config from remote
fi

if [[ "$COMPOSER_GIT_HOOKS" = 'y' ]]; then
  echo "Copying config for brainmaestro/composer-git-hooks"
  # TODO: copy pre-commit script from remote
fi

if [[ "$DEBUGBAR" = 'y' ]]; then
  php artisan vendor:publish --provider="Barryvdh\Debugbar\ServiceProvider"
fi

while [[ $DB_ENCRYPTION != "y" && $DB_ENCRYPTION != "n" ]]; do
  read -p " elgibor-solution/laravel-database-encryption [y/n]: " -e DB_ENCRYPTION
done

while [[ $ELOQUENT_UUID != "y" && $ELOQUENT_UUID != "n" ]]; do
  read -p " goldspecdigital/laravel-eloquent-uuid [y/n]: " -e ELOQUENT_UUID
done

while [[ $LOG_VIEWER != "y" && $LOG_VIEWER != "n" ]]; do
  read -p " opcodesio/log-viewer [y/n]: " -e LOG_VIEWER
done

QUEUE_MONITOR_PACKAGE="romanzipp/laravel-queue-monitor"

while [[ $QUEUE_MONITOR != "y" && $QUEUE_MONITOR != "n" ]]; do
  read -p " $QUEUE_MONITOR_PACKAGE [y/n]: " -e QUEUE_MONITOR
done

PACKAGES=''

if [[ "$DB_ENCRYPTION" = 'y' ]]; then
  PACKAGES="$PACKAGES elgibor-solution/laravel-database-encryption"
fi
if [[ "$QUEUE_MONITOR" = 'y' ]]; then
  PACKAGES="$PACKAGES $QUEUE_MONITOR_PACKAGE"
fi

if [[ "$ELOQUENT_UUID" = 'y' ]]; then
  PACKAGES="$PACKAGES goldspecdigital/laravel-eloquent-uuid"
fi

if [[ "$LOG_VIEWER" = 'y' ]]; then
  PACKAGES="$PACKAGES opcodesio/log-viewer"
fi

if [[ "$PACKAGES" != '' ]]; then
  composer require $PACKAGES
fi

exit 0
